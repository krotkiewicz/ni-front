// taken from https://github.com/Foxandxss/fox-angular-gulp-workflow

var args               = require('yargs').argv;
var gulp               = require('gulp');
var fs                 = require('fs');
var plugins            = require('gulp-load-plugins')();
var es                 = require('event-stream');
var del                = require('del');
var historyApiFallback = require('connect-history-api-fallback');
var _                  = require('lodash');

var vendor             = require('./bower_components/manifest');

vendor.javascript = _.map(vendor.javascript, function(path){
  return 'bower_components/' + path;
});
vendor.css = _.map(vendor.css, function(path){
  return 'bower_components/' + path;
});

var paths = {
  appJavascript:    ['app/js/app.js', 'app/js/**/*.js'],
  appTemplates:     'app/js/**/*.html',
  appMainSass:      'app/scss/*.scss',
  appCss:           'app/css/*.css',
  appStyles:        'app/scss/**/*.scss',
  appImages:        'app/images/**/*',
  appFonts:         'app/fonts/**/*',
  indexHtml:        'app/index.html',
  vendorJavascript: vendor.javascript,
  vendorCss:        vendor.css,
  finalAppJsPaths:   ['/js/vendor.js', '/js/app.js'],
  finalAppCssPath:  '/css/app.css',
  specFolder:       ['spec/**/*_spec.js'],
  distFolder:        'dist',
  distJavascript:    'dist/js',
  distAppJs:         'dist/js/app.js',
  distCss:           'dist/css',
  distImages:        'dist/images',
  distFonts:         'dist/fonts'
};


function isFromBower(fileobj){
  return fileobj.path.indexOf('bower_components') >= 0;
}

function isNotFromBower(fileobj){
  return fileobj.path.indexOf('bower_components') < 0;
}

function buildTemplates() {
  return es.pipeline(
    plugins.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }),
    plugins.angularTemplatecache({
      module: 'app'
    })
  );
}

gulp.task('scripts', function() {
  return gulp.src(paths.vendorJavascript.concat(paths.appJavascript, paths.appTemplates))
    .pipe(plugins.if(/html$/, buildTemplates()))
    .pipe(plugins.if(isFromBower, plugins.concat('vendor.js')))
    .pipe(plugins.if(isNotFromBower, plugins.concat('app.js')))
    .pipe(plugins.if(args.production, plugins.ngAnnotate()))
    .pipe(plugins.if(args.production, plugins.uglify({mangle: true})))
    .pipe(gulp.dest(paths.distJavascript))
    .pipe(plugins.connect.reload());
});

gulp.task('styles', function() {
  var files = paths.vendorCss.concat(paths.appCss).concat(paths.appMainSass);
  return gulp.src(files)
    .pipe(plugins.if(/scss$/, plugins.sass({
    })))
    .pipe(plugins.concat('app.css'))
    .pipe(plugins.if(args.production, plugins.minifyCss({keepSpecialComments: 0})))
    .pipe(gulp.dest(paths.distCss))
    .pipe(plugins.connect.reload());
});

gulp.task('images', function() {
  return gulp.src(paths.appImages)
    .pipe(gulp.dest(paths.distImages))
    .pipe(plugins.connect.reload());
});

gulp.task('fonts', function() {
  return gulp.src(paths.appFonts)
    .pipe(gulp.dest(paths.distFonts))
    .pipe(plugins.connect.reload());
});

gulp.task('indexHtml', ['scripts', 'styles'], function() {
  var manifest = {
    jsfiles: paths.finalAppJsPaths,
    css: paths.finalAppCssPath
  };

  return gulp.src(paths.indexHtml)
    .pipe(plugins.template(manifest))
    .pipe(gulp.dest(paths.distFolder))
    .pipe(plugins.connect.reload());
});


gulp.task('lint', function() {
  return gulp.src(paths.appJavascript.concat(paths.specFolder))
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter('jshint-stylish'));
});

gulp.task('testem', function() {
  return gulp.src(['']) // We don't need files, that is managed on testem.json
    .pipe(plugins.testem({
      configFile: 'testem.json'
    }));
});

gulp.task('watch', ['webserver'], function() {
  gulp.watch(paths.appJavascript, ['lint', 'scripts']);
  gulp.watch(paths.appTemplates, ['scripts']);
  gulp.watch(paths.vendorJavascript, ['scripts']);
  gulp.watch(paths.appImages, ['images']);
  gulp.watch(paths.appFonts, ['fonts']);
  gulp.watch(paths.specFolder, ['lint']);
  gulp.watch(paths.indexHtml, ['indexHtml']);
  gulp.watch(paths.appStyles, ['styles']);
  gulp.watch(paths.appCss, ['styles']);
  gulp.watch(paths.vendorCss, ['styles']);
});

gulp.task('webserver', ['indexHtml', 'images', 'fonts'], function() {
  plugins.connect.server({
    root: paths.distFolder,
    port: 5000,
    livereload: true,
    middleware: function(connect, o) {
        return [ (function() {
            var url = require('url');
            var proxy = require('proxy-middleware');
            var options = url.parse('http://api.n.test:8000/api');
            options.route = '/api';
            //options.preserveHost = true;
            return proxy(options);
        })(), historyApiFallback ];
    }
  });
});

gulp.task('default', ['watch']);
gulp.task('compile', ['indexHtml', 'images', 'fonts']);

