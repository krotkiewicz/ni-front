angular.module('app')
.service('h', function() {
  var self = this;
  this.toDateStr = function(date){
    return moment(date).format('YYYY-MM-DD');
  };
  this.toMonthStr = function(date){
    return moment(date).format('YYYY-MM');
  };
  this.round = function(float){
    return Math.round(float * 100) / 100;
  };
  return this;
});