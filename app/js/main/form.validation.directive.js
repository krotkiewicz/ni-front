angular.module('app')
  .directive('niFormValidate', function($timeout) {
    var flatten = function(object){
      var result = {};
      _.each(object, function(value, key1){
        if(_.isArray(value)){
          result[key1] = value;
        } else {
          _.each(value, function(value, key2){
            result[key1 + '.' + key2] = value;
          });
        }
      });
      return result;
    };

    var disableError = function(input){
      var popoverData = input.data('bs.popover');
      popoverData.enabled = false;
      input.parent().removeClass('has-error');
    };

    var enableError = function(input, error){
      var popoverData = input.data('bs.popover');
      var content = '<span>' + error + '</span>';
      input.parent().addClass('has-error');
      popoverData.enabled = true;
      popoverData.options.content = content;
    };
    return {
      restrict: 'A',
      transclude: true,
      priority: -100,
      template: '<div ng-transclude></div>',
      link: function(scope, element, attrs, controllers){
        var link = function(){
          var inputs = element.find('[name]');

          inputs = _.object(_.map(inputs, function(input) {
            var $input = $(input);
            var name = $input.attr('name');
            if(name){
              $input.popover({
                enabled: false,
                trigger: 'hover',
                html: true,
                content: '',
                placement: 'right'
              });
            }
            name = name || '';
            return [name, $input];
          }));
          delete inputs[''];

          scope.$watch('form.errors', function(newValue, oldValue){
            angular.forEach(inputs, disableError);
            if(!scope.form || !scope.form.errors) {return;}
            var flaterrors = flatten(scope.form.errors);
            angular.forEach(flaterrors, function(errors, name){
              var input = inputs[name];
              enableError(input, errors[0]);
            });
          });
        };
        $timeout(link, 1);
      }
    };
  });
