angular.module('app')
.directive('collapseNav', [ function() {
  return {
    restrict: 'A',
    link: function(scope, ele, attrs) {
      var $a, $aRest, $app, $lists, $listsRest, $nav, $window, Timer, prevWidth, updateClass;
      $window = $(window);
      $lists = ele.find('ul').parent('li');
      $lists.append('<i class="ti-angle-down icon-has-ul-h"></i><i class="ti-angle-double-right icon-has-ul"></i>');
      $a = $lists.children('a');
      $listsRest = ele.children('li').not($lists);
      $aRest = $listsRest.children('a');
      $app = $('#app');
      $nav = $('#nav-container');
      $a.on('click', function(event) {
        var $parent, $this;
        if ($app.hasClass('nav-collapsed-min') || ($nav.hasClass('nav-horizontal') && $window.width() >= 768)) {
          return false;
        }
        $this = $(this);
        $parent = $this.parent('li');
        $lists.not($parent).removeClass('open').find('ul').slideUp();
        $parent.toggleClass('open').find('ul').stop().slideToggle();
        return event.preventDefault();
      });
      $aRest.on('click', function(event) {
        return $lists.removeClass('open').find('ul').slideUp();
      });
      scope.$on('nav:reset', function(event) {
        return $lists.removeClass('open').find('ul').slideUp();
      });
      Timer = void 0;
      prevWidth = $window.width();
      updateClass = function() {
        var currentWidth;
        currentWidth = $window.width();
        if (currentWidth < 768) {
          $app.removeClass('nav-collapsed-min');
        }
        if (prevWidth < 768 && currentWidth >= 768 && $nav.hasClass('nav-horizontal')) {
          $lists.removeClass('open').find('ul').slideUp();
        }
        return prevWidth = currentWidth;
      };
      return $window.resize(function() {
        var t;
        clearTimeout(t);
        return t = setTimeout(updateClass, 300);
      });
    }
  };
}])
.directive('highlightActive', [ function() {
  return {
    restrict: 'A',
    controller: [
      '$scope', '$element', '$attrs', '$location', function($scope, $element, $attrs, $location) {
        var highlightActive, links, path;
        links = $element.find('a');
        path = function() {
          return $location.path();
        };
        highlightActive = function(links, path) {
          path = '#' + path;
          return angular.forEach(links, function(link) {
            var $li, $link, href;
            $link = angular.element(link);
            $li = $link.parent('li');
            href = $link.attr('href');
            if ($li.hasClass('active')) {
              $li.removeClass('active');
            }
            if (path.indexOf(href) === 0) {
              return $li.addClass('active');
            }
          });
        };
        highlightActive(links, $location.path());
        return $scope.$watch(path, function(newVal, oldVal) {
          if (newVal === oldVal) {
            return;
          }
          return highlightActive(links, $location.path());
        });
      }
    ]
  };
}])
.directive('toggleNavCollapsedMin', function($rootScope) {
  return {
    restrict: 'A',
    link: function(scope, ele, attrs) {
      var app;
      app = $('#app');
      return ele.on('click', function(e) {
        if (app.hasClass('nav-collapsed-min')) {
          app.removeClass('nav-collapsed-min');
        } else {
          app.addClass('nav-collapsed-min');
          $rootScope.$broadcast('nav:reset');
        }
        return e.preventDefault();
      });
    }
  };
})
.directive('customPage', function() {
  return {
    restrict: 'A',
    controller: [
      '$scope', '$element', '$location', function($scope, $element, $location) {
        var addBg, path;
        path = function() {
          return $location.path();
        };
        addBg = function(path) {
          $element.removeClass('body-wide body-err body-lock body-auth');
          if (path.indexOf('/activation') === 0) {
            return $element.addClass('body-wide body-auth');
          }
          switch (path) {
            case '/404':
            case '/pages/404':
            case '/pages/500':
              return $element.addClass('body-wide body-err');
            case '/signin':
            case '/signup':
            case '/post-signup':
            case '/forgot-password':
              return $element.addClass('body-wide body-auth');
            case '/pages/lock-screen':
              return $element.addClass('body-wide body-lock');
          }
        };
        addBg($location.path());
        return $scope.$watch(path, function(newVal, oldVal) {
          if (newVal === oldVal) {
            return;
          }
          return addBg($location.path());
        });
      }
    ]
  };
});
