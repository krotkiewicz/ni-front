angular.module('app')
  .directive('niBlink',function($timeout) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.addClass('ni-blink');
        scope.$watch(attrs.niBlink, function (nv, ov) {
          if (nv !== ov) {
            // apply class
            element.addClass('ni-blink-active');

            // auto remove after some delay
            $timeout(function () {
              element.removeClass('ni-blink-active');
            }, 1000);
          }
        });
      }
    };
  });
