angular.module('app')
.filter('pluck', function(){
  return function (input, propname){
    input = input || [];
    return _.pluck(input, propname);
  };
})
.filter('avatar', function(){
  return function (user) {
    return user && user.avatar_url || '/images/default-avatar.png';
  };
})
.filter('joinBy', function () {
  return function (input, delimiter) {
    input = input || [];
    return input.join(delimiter || ',');
  };
})
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var origItem;
        var itemMatches = false;
        var keys = Object.keys(props);

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          origItem = item;

          var text = props[prop].toLowerCase();

          if (prop.indexOf('.') > -1){
            prop = prop.split('.');
            item = item[prop[0]];
            prop = prop[1];
          }

          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(origItem);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});