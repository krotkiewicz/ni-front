(function(window){
  var basePath = '/api/v1';
  var domain = 'api.' + window.location.hostname;
  var protocol = window.location.protocol + '//';
  var port = '';
  if (window.location.port) {
    port = ':' + window.location.port;
  }
  var url = protocol + domain + port + basePath;
  window.apiUrl = url;
})(window);