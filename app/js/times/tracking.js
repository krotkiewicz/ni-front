angular.module('app')
  .controller('TrackingCtrl', function($scope, Tracker){
    $scope.tracker = Tracker;

  })
  .service('Tracker', function ($rootScope, $interval, Rest, localStorageService, h){
    var tracking = null;
    var self = this;
    this.since = null;
    this.last = null;
    this.timeentryid = null;
    this.callback = null;
    this.getTime = function(){
      return h.round(self.last.diff(self.since, 'hours', true));
    };

    this.startTracking = function(timeentryid, since){
      self.timeentryid = timeentryid;
      self.since = since || moment();
      self.last = moment();
      self.iterval = $interval(function(){
        self.last = moment();
      }, 5 * 1000);
      localStorageService.set('tracking', {
        timeentryid: self.timeentryid,
        since: self.since.toDate()
      });
    };
    this.applyTracking = function(){
      var time = self.getTime();
      Rest.all('times').get(self.timeentryid).then(function(timeentry){
        timeentry.time = h.round(timeentry.time + time);
        timeentry.put().then(function(data){
          self.cancelTracking();
          $rootScope.$emit('tracking-applied', {
            timeentryid: timeentry.id,
            addedTime: time
          });
        });
      });
    };
    this.cancelTracking = function(){
      localStorageService.remove('tracking');
      $interval.cancel(self.interval);
      self.interval = null;
      self.since = null;
      self.last = null;
      self.timeentryid = null;
    };

    tracking = localStorageService.get('tracking');
    if (tracking){
      this.startTracking(tracking.timeentryid, moment(tracking.since));
    }
    return this;
  });

