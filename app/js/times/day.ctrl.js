angular.module('app')
  .controller('TimesDayCtrl', function($scope, $rootScope, $modal, $state, $stateParams, Rest, Tracker, AuthService, Session, h){
    $scope.datepickerOpened = false;
    $scope.times = [];
    $scope.currentUser = Session.user;
    $scope.date = moment();
    if ($stateParams.date){
      $scope.date = moment($stateParams.date);
    }
    $scope.date = $scope.date.toDate();
    $scope.$watch('date', function(newValue, oldValue){
      if (newValue === oldValue) {return;}
      var params = {
        date: h.toDateStr($scope.date),
        user: $stateParams.user
      };
      $state.go('li.times_day', params);
    });

    $scope.isToday = function(){
      var d1 = moment($scope.date).startOf('day');
      var d2 = moment().startOf('day');
      return d1.isSame(d2);
    };

    $scope.isMine = function(){
      return $scope.user.id === $scope.currentUser.id;
    };

    $scope.openDatepicker = function(event){
      $scope.datepickerOpened = true;
    };

    $scope.refreshTimes = function(){
      var params = {
        withProject: true,
        user: $scope.user.id,
        date: h.toDateStr($scope.date)
      };
      Rest.all('times').getList(params).then(function(data){
        $scope.times = data;
      });
    };
    $scope.getSum = function(){
      return h.round(_.reduce($scope.times, function(memo, time){
        if (!time.deleted){
          return memo + time.time;
        } else {
          return 0;
        }
      }, 0));
    };

    $scope.user = Session.user;
    if ($stateParams.user){
      if (!AuthService.hasPerm('times.get_timeentry')){
        var params = {
          date: $stateParams.date && h.toDateStr($scope.date) || null,
          user: null
        };
        $state.go('li.times_day', params);
        return;
      }
      Rest.one('users', $stateParams.user).get().then(function(user){
        $scope.user = user;
        $scope.refreshTimes();
      });
    } else {
      $scope.refreshTimes();
    }

    $scope.isModified = function(){
      return _.filter($scope.times, function(time){
        return moment(time.modified_at).isAfter(moment(time.date), 'day');
      }).length > 0;
    };

    $scope.openAddTimeModal = function (project) {
      var modalInstance = $modal.open({
        templateUrl: 'times/addedit.time.modal.html',
        controller: 'AddTimeCtrl',
        resolve: {
          user: function(){
            return $scope.user;
          },
          date: function(){
            return $scope.date;
          }
        }
      });

      modalInstance.result.then(function(newClient) {
        $scope.refreshTimes();
      });
    };

    $scope.openEditTimeModal = function (timeentry) {
      var modalInstance = $modal.open({
        templateUrl: 'times/addedit.time.modal.html',
        controller: 'EditTimeCtrl',
        resolve: {
          timeentry: function(){
            return timeentry;
          }
        }
      });

      modalInstance.result.then(function(newClient) {
        $scope.refreshTimes();
      });
    };

    $scope.startTracking = function(timeentry){
      Tracker.timeentryid = timeentry.id;
      Tracker.startTracking(timeentry.id);
    };

    $scope.applyTracking = function(timeentry){
      Tracker.applyTracking();
    };

    $rootScope.$on('tracking-applied', function(event, data){
      var timeentry = _.find($scope.times, function(time){
        return time.id === data.timeentryid;
      });
      if(timeentry){
        timeentry.time = h.round(timeentry.time + data.addedTime);
      }
    });

    $scope.cancelTracking = function(){
      Tracker.cancelTracking();
    };

    $scope.isTracked = function(timeentry){
      if(timeentry){
        return timeentry.id === Tracker.timeentryid;
      }
      return Tracker.timeentryid !== null;
    };


    $scope.removeEntry = function(timeentry){
      if (!confirm('Are you sure ?')){ return;}
      timeentry.remove().then(function(){
        if($scope.isTracked(timeentry)){
          $scope.cancelTracking();
        }
        $scope.refreshTimes();
      });
    };

    $scope.nextDate = function(){
      $scope.date = moment($scope.date).add(1, 'days').toDate();
    };

    $scope.prevDate = function(){
      $scope.date = moment($scope.date).subtract(1, 'days').toDate();
    };
  });
