angular.module('app')
  .controller('TimesMonthCtrl', function($scope, $q, $state, $stateParams, Rest, Session, h){
    $scope.h = h;
    $scope.today = moment();
    $scope.date = moment();
    if ($stateParams.date){
      $scope.date = moment($stateParams.date);
    }
    $scope.date = $scope.date.toDate();
    $scope.startDate = moment($scope.date).startOf('month');
    $scope.endDate = moment($scope.date).endOf('month');
    $scope.days = [];
    moment().range($scope.startDate, $scope.endDate).by('days', function(moment){
      $scope.days.push(moment);
    });

    var getStartWork = function(user){
      var startDate;
      if (user.profile.start_full_time_work) {
        startDate = moment(user.profile.start_full_time_work);
        if (startDate > $scope.endDate){
          return null;
        }
        startDate = startDate < $scope.startDate ? $scope.startDate : startDate;
        return startDate;
      } else {
        return null;
      }
    };

    var getExpected = function(user){
      var startDate = getStartWork(user);
      var result = {
        month: 0,
        today: 0
      };
      if (!startDate) { return result; }

      moment().range(startDate, $scope.endDate).by('day', function(date){
        if (date.isoWeekday() < 6){
          result.month += 8;
          if (date <= $scope.today){
            result.today += 8;
          }
        }
      });
      return result;
    };
    var getSum = function(userTimes){
      var times = _.values(userTimes);
      return _.reduce(times, function(memo, time) { return memo + (time.time || 0); }, 0);
    };

    var meFirst = function(users){
      var me = _.find(users, function(u) { return u.id === Session.user.id; });
      users = _.filter(users, function(u) { return u.id !== Session.user.id; });
      return [me].concat(users);
    };

    $scope.$watch('date', function(newValue, oldValue){
      if (newValue === oldValue) {return;}
      var params = {
        date: h.toMonthStr($scope.date),
        user: $stateParams.user
      };
      $state.go('li.times_month', params);
    });

    $scope.refresh = function(){
      var promises = [];
      var data = {
        date_start: h.toDateStr($scope.startDate),
        date_end: h.toDateStr($scope.endDate)
      };
      promises.push(Rest.all('times').getList(data));
      promises.push(Rest.all('users').getList({include: 'profile'}));

      $q.all(promises).then(function(data){
        var timesData = data[0];
        $scope.users = meFirst(data[1]);
        $scope.times = {};
        $scope.stats = {};

        // default data:
        _.each($scope.users, function(user){
          var userTimes = $scope.times[user.id] = {};
          var userStats = $scope.stats[user.id] = {};
          var startWork = getStartWork(user);
          userStats.expected = getExpected(user);
          _.each($scope.days, function(day){
            userTimes[day.date()] = {};
            if (startWork && day >= startWork){
              userTimes[day.date()].time = 0;
            }
          });
        });

        // times:
        _.each(timesData, function(time){
          var timeObj = $scope.times[time.user][moment(time.date).date()];
          var modified = moment(time.modified_at).isAfter(moment(time.date), 'day');
          timeObj.time = timeObj.time || 0;
          if (!time.deleted) {
            timeObj.time += time.time;
          }
          timeObj.modified = modified;
        });
        // stats:
        _.each($scope.users, function(user){
          var userStats = $scope.stats[user.id];
          userStats.sum = getSum($scope.times[user.id]);
          userStats.diff = (userStats.sum - userStats.expected.today).toFixed(2);
        });
      });
    };

    $scope.datepickerOpened = false;
    $scope.openDatepicker = function(event){
      $scope.datepickerOpened = true;
    };
    $scope.dateOptions = {
      minMode: 'month'
    };

    $scope.nextDate = function(){
      $scope.date = moment($scope.date).add(1, 'months').toDate();
    };

    $scope.prevDate = function(){
      $scope.date = moment($scope.date).subtract(1, 'months').toDate();
    };

    $scope.refresh();
  });

