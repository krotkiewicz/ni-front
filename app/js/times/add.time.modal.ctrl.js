angular.module('app')
.controller('AddTimeCtrl', function($scope, $modalInstance, Rest, Tracker, h, user, date){
  $scope.title = 'Add time';
  $scope.projects = [];
  $scope.form = {
    errors: null
  };
  $scope.data = {
    project: null,
    time: '',
    description: ''
  };

  Rest.all('projects').getList({include_clients: true}).then(function(data){
    $scope.projects = data;
  });

  $scope.submit = function (data, callback) {
    data = data || angular.copy($scope.data);
    data.project = data.project && data.project.id || null;
    data.user = user.id;
    data.date = h.toDateStr(date);

    Rest.all('times').post(data).then(function(timeentry){
      if(callback){
        callback(timeentry);
      }
      $modalInstance.close();
    }, function(response){
      $scope.form.errors = response.data;
    });
  };

  $scope.track = function(){
    var data = angular.copy($scope.data);
    data.time = data.time || 0;

    $scope.submit(data, function(timeentry){
      Tracker.startTracking(timeentry.id);
    });
  };

  $scope.isToday = function(){
    var d1 = moment(date).startOf('day');
    var d2 = moment().startOf('day');
    return d1.isSame(d2);
  };

  $scope.isTracked = function(){
    return Tracker.timeentryid !== null;
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
