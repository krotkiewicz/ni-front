angular.module('app')
.controller('EditTimeCtrl', function($scope, $modalInstance, Rest, h, timeentry){
  timeentry = Rest.copy(timeentry);
  $scope.title = 'Edit time';
  $scope.projects = [];
  $scope.form = {
    errors: null
  };
  $scope.data = timeentry;

  Rest.all('projects').getList({include_clients: true}).then(function(data){
    $scope.projects = data;
  });

  $scope.submit = function () {
    var project = timeentry.project;
    timeentry.project = timeentry.project && timeentry.project.id || null;

    timeentry.put().then(function(data){
      $modalInstance.close();
    }, function(response){
      timeentry.project = project;
      $scope.form.errors = response.data;
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
