angular.module('app', ['ui.router', 'LocalStorageModule', 'restangular', 'ngCookies', 'ui.bootstrap', 'angular-loading-bar', 'angularFileUpload', 'xeditable', 'pasvaz.bindonce', 'ngSanitize', 'ui.select', 'angular-blocks'])
  .config(function(uiSelectConfig, datepickerConfig, datepickerPopupConfig){
    datepickerConfig.startingDay = 1;
    datepickerPopupConfig.appendToBody = true;
    uiSelectConfig.theme = 'bootstrap';
  })
  .run(function (editableOptions, editableThemes) {
    editableOptions.theme = 'bs3';
    editableThemes.bs3.inputClass = 'input-sm';
  });