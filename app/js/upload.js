angular.module('app')
  .factory('Upload', function($upload) {
    return {
      upload: function(data){
        data.url = apiUrl + '/upload';
        data.method = 'POST';
        return $upload.upload(data);
      }
    };
  });