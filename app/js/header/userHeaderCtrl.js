angular.module('app')
.controller('UserHeaderCtrl', function($scope, $state, Session){
  $scope.user = Session.user;
  $scope.logout = function(){
    Session.destroy();
    $state.go('signin');
  };
});