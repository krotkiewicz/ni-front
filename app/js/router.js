angular.module('app')
  .config(function($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
    $httpProvider.interceptors.push('sessionInjector');
    $urlRouterProvider.otherwise('/users');
    $locationProvider.html5Mode(true);
    $stateProvider
      .state('li', {
        abstract: true,
        templateUrl: 'base.html',
        data: {
          authenticate: true
        }
      })
      .state('li.times_day', {
        url: '/times/day?user&date',
        templateUrl: 'times/day.html',
        data: {
          perm: 'times.manage_own_timeentry'
        }
      })
      .state('li.times_month', {
        url: '/times/month?date',
        templateUrl: 'times/month.html'
      })
      .state('li.presence', {
        url: '/presence?date',
        templateUrl: 'presence/main.html',
        data: {
          perm: 'presence.manage_own_presence'
        }
      })
      .state('li.users', {
        url: '/users',
        template: '<ui-view></ui-view>',
        abstract: true
      })
      .state('li.users.list', {
        url: '',
        templateUrl: 'users/list.html',
        data: {
          perm: 'users.get_user'
        }
      })
      .state('li.users.detail', {
        url: '/:id',
        templateUrl: 'users/profile/main.html'
      })
      .state('li.clients', {
        url: '/clients',
        templateUrl: 'clients/list.html'
      })
      .state('li.client', {
        url: '/clients/:id',
        templateUrl: 'clients/client.html'
      })
      .state('li.project', {
        url: '/clients/:clientId/projects/:id',
        templateUrl: 'clients/projects/project.html'
      })
      .state('li.config', {
        abstract: true,
        url: '/config',
        templateUrl: 'config/base.html',
        data: {
          perm: 'global.view_config'
        }
      })
      .state('li.config.access', {
        url: '/access',
        templateUrl: 'config/access/base.html'
      })
      .state('li.config.other', {
        url: '/other',
        templateUrl: 'config/other/base.html'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'auth/signup.html'
      })
      .state('post-signup', {
        url: '/post-signup',
        templateUrl: 'auth/post-signup.html'
      })
      .state('activation', {
        url: '/activation/:activationKey',
        templateUrl: 'auth/activation.html'
      })
      .state('signin', {
        url: '/signin',
        templateUrl: 'auth/signin.html'
      });
  })
  .run(function ($rootScope, $state, $timeout, AuthService, Session, Restangular, editableOptions, editableThemes) {
    $rootScope.hasPerm = AuthService.hasPerm;
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      if (toState.data && toState.data.authenticate && !AuthService.isAuthenticated()){
        $state.transitionTo('signin');
        event.preventDefault();
      } else if (!s.startsWith(toState.name, 'li') && AuthService.isAuthenticated()) {
        $state.transitionTo('li.users.list');
        event.preventDefault();
      } else if (toState.data && toState.data.perm && !AuthService.hasPerm(toState.data.perm)){
        event.preventDefault();
      }
    });
  });
