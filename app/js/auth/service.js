angular.module('app')
.run(function(Session, localStorageService, Rest){
    var token = localStorageService.get('token');
    var user = localStorageService.get('user');
    if (token && user) {
      Session.setToken(token);
      Session.setUser(user);
      // set user from cache, but at the same time, refresh it:
      Rest.one('users', 'me').get().then(function(data){
        Session.refreshUser(data);
      });
    } else {
      Session.destroy();
    }
})
.service('Session', function ($rootScope, $q, $timeout, localStorageService) {
  var self = this;
  this.user = null;
  this.token = null;

  this.setToken = function(token){
    localStorageService.set('token', token);
    self.token = token;
  };
  this.setUser = function(user){
    localStorageService.set('user', user);
    self.user = user;
    $rootScope.currentUser = user;
  };
  this.refreshUser = function(user){
    /* Quite hacky: we dont want to change reference
     * to user obj - it is already in used
     */
    angular.forEach(user, function(value, key){
      self.user[key] = value;
    });
    localStorageService.set('user', self.user);
  };
  this.destroy = function () {
    self.user = null;
    self.token = null;
    localStorageService.remove('token');
    localStorageService.remove('user');
  };
  return this;
})
.factory('AuthService', function($q, $http, Rest, Session) {
  var authService = {};

  authService.login = function(credentials) {
    var deferred = $q.defer();
    Rest.one('auth').post('', credentials).then(function(data){
      Session.setToken(data.token);
      Rest.one('users', 'me').get().then(function(data){
        Session.setUser(data);
        deferred.resolve();
      });
    }, function(data){
      deferred.reject(data.data);
    });
    return deferred.promise;
  };

  authService.isAuthenticated = function() {
    return !!Session.token;
  };

  authService.hasPerm = function(perm){
    var perms = _.map(Session.user.permissions, function(perm){
      return perm.codename;
    });
    return _.contains(perms, perm);
  };

  return authService;
})
.factory('sessionInjector', function($cookies, Session) {
  return {
    request: function(config) {
      config.headers['X-CSRFToken'] = $cookies.csrftoken;
      if (Session.token) {
        config.headers['Authorization'] = 'Token ' + Session.token;
      }
      return config;
    }
  };
})
.directive('niHasPerm', function(ngIfDirective, AuthService) {
  var ngIf = ngIfDirective[0];

  return {
    transclude: ngIf.transclude,
    priority: ngIf.priority,
    terminal: ngIf.terminal,
    restrict: ngIf.restrict,
    link: function($scope, $element, $attr) {
      var value = $attr['niHasPerm'];
      $attr.ngIf = function() {
        return AuthService.hasPerm(value);
      };
      ngIf.link.apply(ngIf, arguments);
    }
  };
});
