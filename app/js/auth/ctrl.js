angular.module('app')
  .controller('SigninCtrl', function($scope, $state, AuthService) {
    $scope.errors = null;
    $scope.credentials = {
      email: '',
      password: ''
    };
    $scope.login = function(){
      AuthService.login($scope.credentials).then(function(){
        $state.go('li.users.list');
      }, function(data){
        $scope.errors = data;
        $scope.credentials.password = '';
      });
    };
  })
  .controller('ActivationCtrl', function($scope, $state, $stateParams, Rest) {
    var key = $stateParams.activationKey;
    Rest.all('activate').post({key: key}).then(function(){
      $scope.valid = true;
    }, function(){
      $scope.valid = false;
    });
  })
  .controller('SignupCtrl', function($scope, $state, Rest) {
    $scope.form = {
      errors: null
    };
    $scope.credentials = {
      name: '',
      email: '',
      password: '',
      password2: ''
    };

    $scope.signup = function(){
      if ($scope.credentials.password !== $scope.credentials.password2) {
        $scope.form.errors = {
          non_field_errors: ['Passwords don\'t match.']
        };
        return;
      }

      var data = {
        name: $scope.credentials.name,
        email: $scope.credentials.email,
        password: $scope.credentials.password
      };

      Rest.all('signup').post(data).then(function(data){
        $state.go('post-signup');
      }, function(response){
        $scope.form.errors = response.data;
      });

    };
  });
