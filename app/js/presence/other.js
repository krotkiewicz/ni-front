angular.module('app')
  .service('PresenceS', function($interval, $timeout, Rest, Session, h){
    var self = this;
    var syncPresence = true;
    var params;
    this.inOffice = false;
    this.first = moment();
    this.last = moment();
    this.timeSum = 0;
    if (!Session.user) { return; }
    params = {
      user: Session.user.id,
      date: h.toDateStr(moment().toDate())
    };

    this.inWorkSince = function(){
      return self.first.format('h:mm:ss');
    };

    this.inWorkHours = function(){
      return self.last.diff(this.first, 'hours', true).toFixed(2);
    };

    this.hoursToAdd = function(){
      return (self.inWorkHours() - self.timeSum).toFixed(2);
    };

    this.hoursAdded = function(){
      return self.timeSum.toFixed(2);
    };

    this.refreshTimes = function(){
      Rest.all('times').getList(params).then(function(data) {
        self.timeSum = _.reduce(data, function(memo, te){ return memo + te.time; }, 0);
      });
    };

    Rest.all('presences').post({user: Session.user.id}).then(function(data){
      self.inOffice = data.resp.status === 201;
      if (!self.inOffice) { return; }

      Rest.all('presences').getList(params).then(function(data){
        // check when user came to work:
        var first, last;
        if (data.length > 0) {
          first = moment(data[0].ts);
          last = moment(data[1].ts);
          if(first.diff(last) > 0){
            first = last;
          }
          self.first = first;
        }
      });

      Rest.addRequestInterceptor(function(element, operation, what, url){
        // Inform backend about user in office.
        // After each request, notify about presence and ignore next requests for 50 seconds.
        if (syncPresence) {
          syncPresence = false;
          Rest.all('presences').post({user: Session.user.id});
          self.last = moment();
          $timeout(function(){
            syncPresence = true;
          }, 50 * 1000);
        }
        return element;
      });

      self.refreshTimes();
      Rest.addResponseInterceptor(function(data, operation, what){
        // track for any time changes:
        if (what === 'times' && ['put', 'post', 'remove'].indexOf(operation) >= 0){
          self.refreshTimes();
        }
        return data;
      });

    });

    return this;
  });
