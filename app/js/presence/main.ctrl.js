angular.module('app')
  .controller('PresenceCtrl', function($scope, $modal, $state, $stateParams, Rest, AuthService, Session, h){
    $scope.datepickerOpened = false;
    $scope.currentUser = Session.user;
    $scope.date = moment();
    if ($stateParams.date){
      $scope.date = moment($stateParams.date);
    }
    $scope.date = $scope.date.toDate();

    $scope.$watch('date', function(newValue, oldValue){
      if (newValue === oldValue) {return;}
      var params = {
        date: h.toDateStr($scope.date),
        user: $stateParams.user
      };
      $state.go('li.presence', params);
    });

    $scope.openDatepicker = function(event){
      $scope.datepickerOpened = true;
    };

    $scope.refresh = function(){
      var params = {
        date: h.toDateStr($scope.date)
      };
      Rest.all('presences').getList(params).then(function(data){
        var users = {};
        var presences = _.groupBy(data, function(presence){
          users[presence.user.id] = presence.user;
          return presence.user.id;
        });

        presences = _.map(presences, function(userpresences, userid){
          var first, last, tmp;
          first = moment(userpresences[0].ts);
          last = moment(userpresences[1].ts);
          if(first.diff(last) > 0){
            tmp = last;
            last = first;
            first = tmp;
          }
          return {
            'user': users[userid],
            'first': first.toDate(),
            'last': last.toDate()
          };
        });
        $scope.presences = presences;

      });
    };
    $scope.refresh();

    $scope.nextDate = function(){
      $scope.date = moment($scope.date).add(1, 'days').toDate();
    };

    $scope.prevDate = function(){
      $scope.date = moment($scope.date).subtract(1, 'days').toDate();
    };
  });
