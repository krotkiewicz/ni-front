angular.module('app')
  .factory('Rest', function(Restangular) {
    return Restangular.withConfig(function(RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl(apiUrl);
      RestangularConfigurer.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
        var extractedData;
        if (operation === 'getList'){
          extractedData = response.data.results;
          extractedData.count = response.data.count;
          extractedData.next = response.data.next;
          extractedData.previous = response.data.previous;
        } else {
          extractedData = response.data;
        }
        extractedData.resp = {
          status: response.status
        };
        return extractedData;
      });
      RestangularConfigurer.setErrorInterceptor(function(response, deferred, responseHandler){
        if(response.status === 401) {
          Session.destroy();
          $state.transitionTo('signin');
        }
      });
    });
  });