angular.module('app')
.controller('AddClientCtrl', function($scope, $modalInstance, Rest){
  var Clients = Rest.all('clients');
  $scope.form = {
    errors: null
  };
  $scope.client = {
    name: ''
  };

  $scope.add = function () {
    Clients.post($scope.client).then(function(data){
      $modalInstance.close(data);
    }, function(response){
      $scope.form.errors = response.data;
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});