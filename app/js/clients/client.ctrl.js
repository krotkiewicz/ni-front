angular.module('app')
.controller('ClientCtrl', function($scope, $stateParams, $modal, Rest){
  var Client = Rest.one('clients', $stateParams.id);
  $scope.client = null;
  $scope.projects = [];

  Client.get().then(function(data){
    $scope.client = data;
  });

  $scope.refreshList = function(){
    Rest.all('projects').getList({client: $stateParams.id}).then(function(data){
      $scope.projects = data;
    });
  };

  $scope.openAddProject = function () {
    var modalInstance = $modal.open({
      templateUrl: 'clients/projects/add.html',
      controller: 'AddProjectCtrl'
    });
    modalInstance.result.then(function(newProject){
      $scope.refreshList();
    });
  };

  $scope.remove = function(project){
    if (!confirm('Are you sure ?')){ return;}
    project.remove().then(function(){
      $scope.refreshList();
    });
  };

  $scope.openUpdateProject = function (project) {
    var modalInstance = $modal.open({
      templateUrl: 'clients/projects/update.html',
      controller: 'UpdateProjectCtrl',
      resolve: {
        project: function(){
          return project;
        }
      }
    });

    modalInstance.result.then(function(newClient) {
      $scope.refreshList();
    });
  };

  $scope.refreshList();
});
