angular.module('app')
.controller('UpdateClientCtrl', function($scope, $modalInstance, Rest, client){
  var Clients = Rest.all('clients');
  $scope.form = {
    errors: null
  };

  $scope.client = Rest.copy(client);

  $scope.update = function () {
    $scope.client.put().then(function(response){
      $modalInstance.close();
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});