angular.module('app')
.controller('AddProjectCtrl', function($scope, $stateParams, $modalInstance, Rest){
  $scope.form = {
    errors: null
  };
  $scope.project = {
    client: $stateParams.id,
    name: ''
  };

  $scope.add = function () {
    Rest.all('projects').post($scope.project).then(function(data){
      $modalInstance.close(data);
    }, function(response){
      $scope.form.errors = response.data;
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
