angular.module('app')
.controller('ProjectCtrl', function($scope, $stateParams, Rest){
  var Project = Rest.one('projects', $stateParams.id);
  $scope.project = null;

  Project.get().then(function(data){
    $scope.project = data;
  });
});