angular.module('app')
.controller('UpdateProjectCtrl', function($scope, $modalInstance, Rest, project){
  $scope.form = {
    errors: null
  };

  $scope.project = Rest.copy(project);

  $scope.update = function () {
    $scope.project.put().then(function(response){
      $modalInstance.close();
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});