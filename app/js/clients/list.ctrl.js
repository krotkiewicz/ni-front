angular.module('app')
.controller('ClientListCtrl', function($scope, $modal, Rest){
  $scope.clients = [];

  $scope.refreshList = function(){
    Rest.all('clients').getList().then(function(data){
      $scope.clients = data;
    });
  };

  $scope.openAddClient = function () {
    var modalInstance = $modal.open({
      templateUrl: 'clients/add.html',
      controller: 'AddClientCtrl'
    });

    modalInstance.result.then(function(newClient) {
      $scope.refreshList();
    });
  };

  $scope.remove = function(client){
    if (!confirm('Are you sure ?')){ return;}
    client.remove().then(function(){
      $scope.refreshList();
    });
  };

  $scope.openUpdateClient = function (client) {
    var modalInstance = $modal.open({
      templateUrl: 'clients/update.html',
      controller: 'UpdateClientCtrl',
      resolve: {
        client: function(){
          return client;
        }
      }
    });

    modalInstance.result.then(function(newClient) {
      $scope.refreshList();
    });
  };

  $scope.refreshList();
});
