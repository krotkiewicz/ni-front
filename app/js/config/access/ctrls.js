angular.module('app')
  .controller('InviteByEmailCtrl', function($scope, $modal, Rest){
    $scope.search = '';

    var refresh = function(){
      Rest.all('invitations').getList().then(function(data){
        $scope.invitations = data;
      });
    };

    $scope.delete = function(invitation){
      if (!confirm('Are you sure ?')){ return;}
      invitation.remove().then(function(){
        refresh();
      });

    };

    $scope.openAddModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'config/access/invitation.modal.html',
        controller: 'AddInvitationCtrl'
      });

      modalInstance.result.then(function(invitation) {
        refresh();
      });
    };

    $scope.edit = function (invitation) {
      var modalInstance = $modal.open({
        templateUrl: 'config/access/invitation.modal.html',
        controller: 'EditInvitationCtrl',
        resolve: {
          invitation: function(){
            return invitation;
          }
        }
      });
      modalInstance.result.then(function(invitation) {
        refresh();
      });
    };
    refresh();
  })
  .controller('_BaseAccessAddCtrl', function($scope, Rest){
    $scope.form = {
      errors: null
    };
    $scope.data = {};
    $scope.data.all_clients = true;
    $scope.data.groups = [];
    $scope.data.clients = [];
    $scope.data.projects = [];

    $scope.projects = [];
    $scope.clients = [];

    $scope.$watch('data.all_clients', function(newVal, oldVal){
      if (newVal === true){
        $scope.data.clients = [];
        $scope.data.projects = [];
      }
    });

    Rest.all('projects').getList({include_clients: true}).then(function(data){
      $scope.projects = data;
      $scope.clients = _.map(data, function(project){return project.client;});
      $scope.clients = _.uniq($scope.clients, function(c){return c.id;});
    });

    Rest.all('groups').getList().then(function(data){
      $scope.groups = data;
    });

    $scope.groupProjectsFn = function(project){
      return project.client.name;
    };

  })
  .controller('AddInvitationCtrl', function($scope, $controller, $modal, Rest, $modalInstance){
    var self = this;
    angular.extend(this, $controller('_BaseAccessAddCtrl', {$scope: $scope}));
    $scope.title = 'Invite';
    $scope.data.email = '';

    this.prepareData = function(){
      var data = angular.copy($scope.data);

      if ($scope.data.all_clients) {
        data.clients = [];
        data.projects = [];
      }
      return data;
    };

    $scope.submit = function(){
      var data = self.prepareData();

      Rest.all('invitations').post(data).then(function(data){
        $modalInstance.close();
      }, function(response){
        $scope.form.errors = response.data;
      });
    };

    $scope.cancel = function(){
      $modalInstance.close();
    };
  })
  .controller('EditInvitationCtrl', function($scope, $controller, $modal, Rest, $modalInstance, invitation){
    var self = this;
    angular.extend(this, $controller('AddInvitationCtrl', {$scope: $scope, $modalInstance: $modalInstance}));
    $scope.title = 'Edit Invitation';
    $scope.emailDisabled = true;
    $scope.data.email = invitation.email;
    $scope.data.all_clients = invitation.all_clients;
    $scope.data.groups = _.pluck(invitation.groups, 'id');
    $scope.data.clients = _.pluck(invitation.clients, 'id');
    $scope.data.projects = _.pluck(invitation.projects, 'id');

    $scope.submit = function(){
      var data = self.prepareData();
      var newInvitation =  Rest.copy(invitation);
      angular.extend(newInvitation, data);

      newInvitation.put().then(function(data){
        $modalInstance.close();
      }, function(response){
        $scope.form.errors = response.data;
      });
    };

  })
  .controller('DomainAccessCtrl', function($scope, $controller, $modal, Rest){
    var refresh = function(){
      Rest.all('domainaccess').getList().then(function(data){
        $scope.accessobjs = data;
      });
    };

    $scope.delete = function(accessobj){
      if (!confirm('Are you sure ?')){ return;}
      accessobj.remove().then(function(){
        refresh();
      });

    };

    $scope.openAddModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'config/access/accessobj.modal.html',
        controller: 'AddDomainAccessCtrl'
      });

      modalInstance.result.then(function(accessobj) {
        refresh();
      });
    };

    $scope.edit = function (accessobj) {
      var modalInstance = $modal.open({
        templateUrl: 'config/access/accessobj.modal.html',
        controller: 'EditDomainAccessCtrl',
        resolve: {
          accessobj: function(){
            return accessobj;
          }
        }
      });
      modalInstance.result.then(function(accessobj) {
        refresh();
      });
    };
    refresh();
  })
  .controller('AddDomainAccessCtrl', function($scope, $controller, $modal, Rest, $modalInstance){
    var self = this;
    angular.extend(this, $controller('_BaseAccessAddCtrl', {$scope: $scope}));
    $scope.title = 'Add Domain';
    $scope.data.domain = '';

    this.prepareData = function(){
      var data = angular.copy($scope.data);

      if ($scope.data.all_clients) {
        data.clients = [];
        data.projects = [];
      }
      return data;
    };

    $scope.submit = function(){
      var data = self.prepareData();
      Rest.all('domainaccess').post(data).then(function(data){
        $modalInstance.close();
      }, function(response){
        $scope.form.errors = response.data;
      });
    };

    $scope.cancel = function(){
      $modalInstance.close();
    };

  })
  .controller('EditDomainAccessCtrl', function($scope, $controller, $modal, Rest, $modalInstance, accessobj){
    var self = this;
    angular.extend(this, $controller('AddDomainAccessCtrl', {$scope: $scope, $modalInstance: $modalInstance}));
    $scope.title = 'Edit Domain';
    $scope.data.domain = accessobj.domain;
    $scope.data.all_clients = accessobj.all_clients;
    $scope.data.groups = _.pluck(accessobj.groups, 'id');
    $scope.data.clients = _.pluck(accessobj.clients, 'id');
    $scope.data.projects = _.pluck(accessobj.projects, 'id');

    $scope.submit = function(){
      var data = self.prepareData();
      var newAccessobj =  Rest.copy(accessobj);
      angular.extend(newAccessobj, data);

      newAccessobj.put().then(function(data){
        $modalInstance.close();
      }, function(response){
        $scope.form.errors = response.data;
      });
    };
  });

