angular.module('app')
.controller('RolesCtrl', function($scope, $modal, Rest){
  $scope.roles = [];

  $scope.save = function(newval, role){
    role = Rest.copy(role);
    role.name = newval;
    role.put();
  };

  $scope.openModal = function () {
    var modalInstance = $modal.open({
      templateUrl: 'config/other/roles/add.modal.html',
      controller: 'AddRoleCtrl'
    });

    modalInstance.result.then(function(role) {
      $scope.refresh();
    });
  };

  $scope.delete = function(role){
    var r = confirm('Are you sure ?');
    if (r === true) {
      role.remove().then(function(){
        $scope.refresh();
      });
    }
  };

  $scope.refresh = function(){
    Rest.all('roles').getList().then(function(data){
      $scope.roles = data;
    });
  };
  $scope.refresh();
})
.controller('AddRoleCtrl', function($scope, $modalInstance, Rest){
  $scope.form = {
    errors: null
  };
  $scope.role = {
    name: ''
  };

  $scope.add = function () {
    Rest.all('roles').post($scope.role).then(function(data){
      $modalInstance.close(data);
    }, function(response){
      $scope.form.errors = response.data;
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
