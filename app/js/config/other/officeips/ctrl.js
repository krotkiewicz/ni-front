angular.module('app')
  .controller('OfficeIpsCtrl', function($scope, $modal, Rest){
    $scope.officeips = [];

    $scope.save = function(newval, officeip){
      officeip = Rest.copy(officeip);
      officeip.ip = newval;
      officeip.put();
    };

    $scope.openModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'config/other/officeips/add.modal.html',
        controller: 'AddOfficeIpCtrl'
      });

      modalInstance.result.then(function(officeip) {
        $scope.refresh();
      });
    };

    $scope.delete = function(officeip){
      var r = confirm('Are you sure ?');
      if (r === true) {
        officeip.remove().then(function(){
          $scope.refresh();
        });
      }
    };

    $scope.refresh = function(){
      Rest.all('officeips').getList().then(function(data){
        $scope.officeips = data;
      });
    };
    $scope.refresh();
  })
  .controller('AddOfficeIpCtrl', function($scope, $modalInstance, Rest){
    $scope.form = {
      errors: null
    };
    $scope.officeip = {
      ip: ''
    };

    $scope.add = function () {
      Rest.all('officeips').post($scope.officeip).then(function(data){
        $modalInstance.close(data);
      }, function(response){
        $scope.form.errors = response.data;
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });
