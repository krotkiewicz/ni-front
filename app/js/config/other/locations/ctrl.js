angular.module('app')
.controller('LocationsCtrl', function($scope, $modal, Rest){
  $scope.locations = [];

  $scope.save = function(newval, location){
    location = Rest.copy(location);
    location.name = newval;
    location.put();
  };

  $scope.openModal = function () {
    var modalInstance = $modal.open({
      templateUrl: 'config/other/locations/add.modal.html',
      controller: 'AddLocationCtrl'
    });

    modalInstance.result.then(function(location) {
      $scope.refresh();
    });
  };

  $scope.delete = function(location){
    var r = confirm('Are you sure ?');
    if (r === true) {
      location.remove().then(function(){
        $scope.refresh();
      });
    }
  };

  $scope.refresh = function(){
    Rest.all('locations').getList().then(function(data){
      $scope.locations = data;
    });
  };
  $scope.refresh();
})
.controller('AddLocationCtrl', function($scope, $modalInstance, Rest){
  $scope.form = {
    errors: null
  };
  $scope.location = {
    name: ''
  };

  $scope.add = function () {
    Rest.all('locations').post($scope.location).then(function(data){
      $modalInstance.close(data);
    }, function(response){
      $scope.form.errors = response.data;
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
