angular.module('app')
.controller('UserCtrl', function($scope, $stateParams, Rest){
  $scope.user = null;
  $scope.userprofile = null;
  $scope.accessprofile = null;

  $scope.refresh = function(){
    var params = {
      'include': ['groups']
    };
    Rest.one('users', $stateParams.id).get(params).then(function(data){
      $scope.user = data;
    });
    Rest.one('userprofiles', $stateParams.id).get().then(function(data){
      $scope.userprofile = data;
    });
    Rest.one('accessprofiles', $stateParams.id).get().then(function(data){
      $scope.accessprofile = data;
    });
  };

  $scope.$on('USER-CHANGED', function(){
    $scope.refresh();
  });
  $scope.refresh();
});
