angular.module('app')
.controller('PersonalInfoCtrl', function($scope, $stateParams, Rest, $modal){
  $scope.openModal = function () {
    var modalInstance = $modal.open({
      templateUrl: 'users/profile/personalinfo/edit.html',
      controller: 'EditPersonalInfoCtrl',
      windowClass: 'modal-large',
      resolve: {
        user: function(){
          return $scope.user;
        },
        userprofile: function(){
          return $scope.userprofile;
        }
      }
    });

    modalInstance.result.then(function() {
      $scope.$emit('USER-CHANGED');
    });
  };
})
.controller('EditPersonalInfoCtrl', function($scope, $filter, $modalInstance, Upload, Rest, Session, h, user, userprofile){
  $scope.userprofile = Rest.copy(userprofile);
  $scope.user = user;
  $scope.userprofile.roles = _.map($scope.userprofile.roles, function(r){return r.id;});
  $scope.userprofile.location = $scope.userprofile.location && $scope.userprofile.location.id;

  $scope.locations = [];
  $scope.roles = [];
  Rest.all('locations').getList().then(function(data){
    $scope.locations = data;
  });
  Rest.all('roles').getList().then(function(data){
    $scope.roles = data;
  });

  $scope.form = {};
  $scope.datepickers = {
    birthdate: false,
    start_work: false,
    start_full_time_work: false
  };

  $scope.openDatepicker = function(field, $event){
    $event.preventDefault();
    $event.stopPropagation();
    $scope.datepickers[field] = true;
  };

  $scope.avatar_url = $filter('avatar')($scope.userprofile);
  $scope.profileImageChanged = function(files){
    $scope.upload = Upload.upload({
      file: files[0]
    }).success(function(data, status, headers, config){
      $scope.avatar_url = data.url;
      $scope.userprofile.avatar = data.id;
    });
  };

  $scope.submit = function () {
    var up = Rest.copy($scope.userprofile);
    _.forEach(['birthdate', 'start_work', 'start_full_time_work'], function(f){
      up[f] = up[f] && h.toDateStr(up[f]) || null;
    });
    up.put().then(function(){
      if(user.id === Session.user.id){
        Session.refreshUser({
          avatar_url: $scope.avatar_url
        });
      }
      $modalInstance.close();
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});

