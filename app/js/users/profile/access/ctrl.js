angular.module('app')
.controller('UserAccessCtrl', function($scope, $stateParams, $modal){
  $scope.openModal = function () {
    var modalInstance = $modal.open({
      templateUrl: 'users/profile/access/edit.html',
      controller: 'EditUserAccessCtrl',
      resolve: {
        $callerScope: function(){
          return $scope;
        }
      }
    });
  };
})
.controller('EditUserAccessCtrl', function($scope, Rest, $callerScope){
    $scope.$callerScope = $callerScope;
})
.controller('EditUserGroupCtrl', function($scope, Rest){
  var $callerScope = $scope.$callerScope;
  var user = $callerScope.user;

  user = Rest.copy(user);

  $scope.data = {
    groups: _.pluck(user.groups, 'id'),
  };
  $scope.groups = [];

  Rest.all('groups').getList().then(function(data){
    $scope.groups = data;
  });

  $scope.save = function () {
    user.patch({groups: $scope.data.groups || []}).then(function(){
      $callerScope.$emit('USER-CHANGED');
    });
  };
})
.controller('EditUserAccessClientsCtrl', function($scope, Rest){
    var $callerScope = $scope.$callerScope;
    var accessprofile = $callerScope.accessprofile;

    accessprofile = Rest.copy(accessprofile);
    accessprofile.clients = _.pluck(accessprofile.clients, 'id');
    accessprofile.projects = _.pluck(accessprofile.projects, 'id');

    $scope.data = {
      accessprofile: accessprofile
    };

    $scope.$watch('data.accessprofile.all_clients', function(newVal, oldVal){
      if (newVal === true){
        $scope.data.accessprofile.clients = [];
        $scope.data.accessprofile.projects = [];
      }
    });

    Rest.all('projects').getList({include_clients: true}).then(function(data){
      $scope.projects = data;
      $scope.clients = _.map(data, function(project){return project.client;});
      $scope.clients = _.uniq($scope.clients, function(c){return c.id;});
    });

    $scope.save = function(){
      accessprofile.put().then(function(){
        $callerScope.$emit('USER-CHANGED');
      });
    };

});

