angular.module('app')
.controller('UsersCtrl', function($scope, $filter, Rest){
  $scope.users = [];
  $scope.pageSizeOpt = [10, 50, 100];
  $scope.pageSize = $scope.pageSizeOpt[0];
  $scope.rowOrder = 'name';
  $scope.searchKeywords = '';
  $scope.currentPage = 1;

  $scope.changeOrder = function(rowName) {
    if ($scope.rowOrder !== rowName) {
      $scope.rowOrder = rowName;
      $scope.currentPage = 1;
      return $scope.onParamChange();
    }
  };

  $scope.changeSearch = function () {
    $scope.currentPage = 1;
    $scope.onParamChange();
  };

  $scope.selectPage = function(){
    $scope.onParamChange();
  };

  $scope.onPageSizeChange = function () {
    $scope.currentPage = 1;
    $scope.onParamChange();
  };

  $scope.onParamChange = function(){
    var params = {
      page_size: $scope.pageSize,
      ordering: $scope.rowOrder,
      page: $scope.currentPage,
      include: 'profile'
    };
    if ($scope.searchKeywords.length > 2){
      params['search'] = $scope.searchKeywords;
    }
    Rest.all('users').getList(params).then(function(data){
      $scope.users = data;
    });
  };
  $scope.onParamChange();
});
