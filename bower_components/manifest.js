exports.javascript = [
  'lodash/dist/lodash.js',
  'momentjs/moment.js',
  'jquery/dist/jquery.js',
  'bootstrap/dist/js/bootstrap.js',
  'angular/angular.js',
  'restangular/dist/restangular.js',
  'angular-ui-router/release/angular-ui-router.js',
  'angular-cookies/angular-cookies.js',
  'angular-local-storage/dist/angular-local-storage.js',
  'angular-bootstrap/ui-bootstrap.js',
  'angular-bootstrap/ui-bootstrap-tpls.js',
  'angular-loading-bar/build/loading-bar.js',
  'ng-file-upload/angular-file-upload.js',
  'angular-xeditable/dist/js/xeditable.js',
  'angular-bindonce/bindonce.js',
  'angular-sanitize/angular-sanitize.js',
  'angular-ui-select/dist/select.js',
  'angular-blocks/dist/angular-blocks.js',
  'underscore.string/dist/underscore.string.js',
  'moment-range/lib/moment-range.js'
];

exports.css = [
  'angular-loading-bar/build/loading-bar.css',
  'angular-ui-select/dist/select.css'
];
